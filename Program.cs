﻿using System;
using System.Security.Cryptography.X509Certificates;

string appName = Console.ReadLine();

if (appName == "1")
    {
        int maxWeight = 0;
        int[] weights = { 2, 5, 3, 4, 10 };

        Console.WriteLine($"Max weight (array): {weights.Max()}");

        // Updated: for with one if instead of multiple nested ifs
        for (int i = 0; i < weights.Length; i++)
        {
            if (weights[i] >= maxWeight)
            {
                maxWeight = weights[i];
            }
        }
    }

if (appName == "2")
    {
        string nameFromUser = Console.ReadLine();
        // Updated: myName stored as const in lower case
        const string myName = "anton";

        if (nameFromUser.ToLower() == myName)
        {
            // Updated: to preserve nice formatting, make first letter capital
            Console.WriteLine($"Hello, {string.Concat(myName[..1].ToUpper(), myName[1..])}!");
        }
        else
        {
            Console.WriteLine($"Hey, what's up {nameFromUser}!");
        }
    }

if (appName == "3")
    {
        // Updated: added conversion to int right here
        int varOne = Int16.Parse(Console.ReadLine());
        int varTwo = Int16.Parse(Console.ReadLine());
        int varThree = Int16.Parse(Console.ReadLine());

        int[] allVars = { varOne, varTwo, varThree };
        Console.WriteLine($"Average (array): {allVars.Average()}");
        
        // Updated: mistype fixed
        double averageVal = (varOne + varTwo + varThree) / 3.0;
        Console.WriteLine($"Average (sum and divide): {averageVal}");
    }